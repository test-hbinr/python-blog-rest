import os
import logging
from logging import handlers
# from config import basedir
from flask import Flask
from config import load_config  # 绝对导入
# from flask_sqlalchemy import SQLAlchemy
# from flask_restful import reqparse, abort, Api, Resource
app = Flask(__name__)
# api = Api(app)
# app.config.from_object('config')
config = load_config()
app.config.from_object(config)
from app.site import wikis
from app.user_manager import user_manager
from app.make_number import make_number
app.register_blueprint(wikis, url_prefix='/wiki')
app.register_blueprint(user_manager, url_prefix='/user')
app.register_blueprint(make_number, url_prefix='/mn')

format_str = logging.Formatter(app.config.get('LOG_FORMAT'))
th = handlers.TimedRotatingFileHandler(filename=app.config.get('LOG_FILENAME'), when='D', backupCount=0,
                                       encoding='utf-8')  # 往文件里写入#指定间隔时间自动生成文件的处理器
th.setFormatter(format_str)  # 设置文件里写入的格式
app.logger.addHandler(th)
app.logger.setLevel(app.config.get('LOG_LEVEL'))
app.logger.info('app init commit')
