#!/usr/bin/python
# -*- coding: UTF-8 -*-
# @License: Copyright © 2019 代鹏. All rights reserved.
# @Author: 代鹏
# @Contact: 272481080@qq.com
# @File: http_beta.py
# @Date: 2019/7/17
# @Software：kk
# @description :
from flask import make_response


# 添加跨域header
def add_headers(massage):
    rst = make_response(massage)
    rst.headers['Access-Control-Allow-Origin'] = "*"
    rst.headers['Access-Control-Allow-Credentials'] = 'true'
    # rst.headers['Access-Control-Allow-Methods'] = 'PUT,GET,POST,DELETE'
    allow_headers = "Referer,Accept,Origin,User-Agent,Content-Type"
    rst.headers['Access-Control-Allow-Headers'] = allow_headers
    return rst
