#!/usr/bin/python
# -*- coding: UTF-8 -*-
# @License: Copyright © 2019 代鹏. All rights reserved.
# @Author: 代鹏
# @Contact: 272481080@qq.com
# @File: make_number.py
# @Date: 2019/9/09
# @Software：cu
# @description :make up the number 凑数、充数
from flask import jsonify, request, Blueprint
from app.server.http_beta import add_headers
# from app.server.cu_logger import Logger
import sys
# import logging
from app import app
import itertools as it
import numpy as np
import time
from app.server import cu_mongodb_conn

# logging.basicConfig(level=app.config.get('LOG_LEVEL'),
#                     filename=app.config.get('LOG_FILENAME'),
#                     filemode=app.config.get('LOG_FILEMODE'),
#                     format=app.config.get('LOG_FORMAT')
#                     )
# log = Logger()
make_number = Blueprint('make_number', __name__)
# 最小差额平方，预制一个大数
min1 = sys.float_info.max
# 最终列表
R = []


def init():
    global min1, R
    min1 = sys.float_info.max
    R = []


@make_number.route('/mn/getCounter', methods=['GET', 'POST'])
def get_counter():
    p = {'_id': 'tools-make-number'}
    counter = cu_mongodb_conn.find_one('tools_counter', p)
    res = dict()
    resp = dict()
    if counter:
        resp['status'] = 0
        res['used_num'] = counter['used_num']
        resp['data'] = res
    else:
        resp['status'] = 1
        resp['data'] = 'airball'
    return add_headers(jsonify(resp))


@make_number.route('/mn', methods=['GET', 'POST'])
def make_n():
    init()  # 初始化
    data = request.get_json()
    datas = data['datas']
    f_datas = []
    for d in datas.split(','):
        f_datas.append(float(d))
    amount = float(data['amount'])
    app.logger.info('amount:%s data_set:%s' % (amount, datas))
    resp = dict()
    if f_datas and amount:
        # exec_mn(f_datas, amount)
        exec_mn_new(f_datas, amount)
        r_sum = sum(R[0])
        r_min = r_sum - amount
        r_str = ''
        for r in R:
            r_str = r_str + '组合:' + ','.join('%s' % i for i in r) + '\n'
        if R:
            resp['status'] = 0
            data = dict()
            data['result'] = r_str
            data['r_sum'] = r_sum
            data['r_min'] = r_min
            resp['data'] = data
        else:
            resp['status'] = 1
            resp['msg'] = 'a system error dccurred!'
    else:
        resp['status'] = 1
        resp['msg'] = 'the parameters error!'
    app.logger.info('Complete the task,the result is :%s' % (jsonify(resp)))
    counter()
    return add_headers(jsonify(resp))


# def exec_mn(datas, tar):
#     # 金额列表
#     A = datas
#     # A = [243.48, 16.97, 1986.26, 740.4, 360, 2086.94, 372, 789.25, 260.37, 28.15, 400, 1072, 1020, 6600, 2332, 11640, 2,
#     #      224.54, 19.59, 360, 2023.9, 788.93, 12.06, 5346]
#     # 目标金额
#     target = tar
#
#     # target = 12535.67
#
#     def abc(B, TR, nu):
#         # 嵌套函数
#         # B:订单列表
#         # TR:临时列表
#         # nu:列表下标，也是递归层级
#         # 确保每一层的列表独立
#         b = B
#         tr = TR[:]
#         global min1, R
#         for j in range(len(b)):
#             if min1 == 0:
#                 break
#             tr[nu] = b[j]  # 向列表中增加一个数
#             if nu < num - 1:
#                 abc(b[j + 1:], tr, nu + 1)
#             else:
#                 min2 = (sum(tr) - target) ** 2
#                 if min2 < min1:  # 如果找到更小的就替换
#                     R = tr[:]
#                     min1 = min2
#                     # print('update R:%s,min:%s' % (R, min1))  # 输出替换的数值
#                     app.logger.info('update Result:%s,min:%s' % (R, min1))
#
#     # 开始
#     for i in range(1, len(A) + 1):
#         if min1 == 0:
#             break
#         num = i
#         TR = [0] * num
#         print('TR:%s' % TR)
#         app.logger.info('change len, new TR:%s' % TR)
#         # abc(A[:], TR, 0)
#         abc(A, TR, 0)
#     # print('min1:%s' % min1)
#     # print('list:%s' % R)
#     # print('sum:%s' % sum(R))
#     app.logger.info('last, min1:%s list:%s sum:%s' % (min1, R, sum(R)))


def exec_mn_new(datas, tar):
    # 新方法
    global min1, R
    datas.sort(reverse=True)  # 降序排列
    d_min = datas[len(datas) - 1]
    if d_min >= tar:  # 如果最小数仍然比目标数大。直接返回最小数即可，不必再计算
        R.append([d_min])
        min1 = abs(d_min - tar)
        app.logger.info('最小数大于目标数，跳过计算返回最小数')
        return
    app.logger.info('不符合第一种情况：最小数大于目标数，计算第二种情况')
    # 如果最大数小于目标数，则最大数就是最接近目标数的那个数。
    # 同理，如果最大的两位数的和小于目标数，则这两个数的和就是最接近目标数的那个组合。
    # 三个数及以上同理。
    i_sum = 0
    tmp_result = []
    tmp_result_len = 0
    for i in datas:  # datas已经排序，第一次取到最大数
        i_sum = i_sum + i  # 第二个数和前一个数相加得到“两个最大数的和”
        if i_sum <= tar:
            min1 = tar - i_sum
            tmp_result.append(i)
            tmp_result_len = len(tmp_result)
            app.logger.info('临时集合中添加一个数%s,差额：%s,当前集合：%s' % (i, min1, tmp_result))
        else:
            if tmp_result_len > 0:
                R.append(tmp_result)
                app.logger.info('临时集合的和接近目标数,终止第二种情况,临时集合状态如上')
                break
    if tmp_result_len == len(datas):
        R.append(tmp_result)
        app.logger.info('第二种情况完成，临时集合的和仍然小于或等于目标数，临时集合最接近目标数，返回临时集合')
        return
    app.logger.info('进入终极运算，即将调用mn_numpy')
    for i2 in range(tmp_result_len + 1, len(datas)):
        mn_numpy(datas, tar, i2)


def mn_numpy(data_set, target_data, data_number):
    global min1, R
    start = time.perf_counter()
    app.logger.info('执行mn_numpy, data_number:%s' % data_number)
    a_set = it.combinations(data_set, data_number)  # 组合
    a_set = np.array(list(a_set))  # 组合转化为numpy矩阵
    a_set_sum = np.sum(a_set, axis=1)  # 按行求和，即每种组合的和
    app.logger.info('组合求和结果, a_set_sum:%s ' % a_set_sum)
    a_set_abs = np.abs(a_set_sum - target_data)  # 和目标数比较
    a_set_abs_min = np.amin(a_set_abs)  # 最小值
    if a_set_abs_min <= min1:  # 是最接近的组合
        a_set_winner = np.argwhere(a_set_abs == np.amin(a_set_abs))  # 差值最小的组合的坐标（索引）
        app.logger.info('a_set_winner 坐标:%s' % a_set_winner)
        tmp_result = a_set[a_set_winner.flatten()].tolist()
        app.logger.info('a_set_winner 坐标对应的组合:%s' % tmp_result)
        if a_set_abs_min < min1:  # 这个组合比前一个组合更优，清掉前一个组合的记录
            R = []
        R.extend(tmp_result)
        min1 = a_set_abs_min
        app.logger.info('in mn_numpy R:%s' % R)
    stop = time.perf_counter()
    running_time = stop - start
    app.logger.info("running time:%s" % running_time)


# 计数
def counter():
    p = {'_id': 'tools-make-number'}
    cu_mongodb_conn.update_one('tools_counter', p, {'$inc': {'used_num': 1}}, True)


if __name__ == "__main__":
    # 第一种情况，最小数，单数
    # ds = [1000]
    # t = 900
    # 第一种情况，最小数，多数
    # ds = [100, 200, 300, 400, 500]
    # t = 9
    # 普通数
    # ds = [100, 200, 300, 400, 500]
    # t = 600
    # 总和等于目标数
    # ds = [100, 200, 300, 400, 500]
    # t = 1500
    # 总和大于目标数
    ds = [100, 200, 300, 400, 500]
    t = 1600
    # 大计算量
    # ds = [8400, 6300, 6195, 8610, 13650, 12600, 1296, 24725, 8400, 13125, 6038, 15750, 8033, 8925, 4725, 8190, 10500,
    #          12600, 10500, 6300, 8400, 12590, 15120, 11340, 9450]
    # t = 199970

    exec_mn_new(ds, t)
    print('最小差min1:%s' % min1)
    print('组合R:%s' % R)
