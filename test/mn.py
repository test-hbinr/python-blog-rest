#!/usr/bin/python
# -*- coding: UTF-8 -*-
# @License: Copyright © 2019 代鹏. All rights reserved.
# @Author: 代鹏
# @Contact: 272481080@qq.com
# @File: mongo.py
# @Date: 2021/10/09
# @Software：kk
# @description :
# -*- coding: utf-8 -*-
import itertools as it
import numpy as np
import time

# list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9.1, 0]
list1 = [8400, 6300, 6195, 8610, 13650, 12600, 1296, 24725, 8400, 13125, 6038, 15750, 8033, 8925, 4725, 8190, 10500,
         12600, 10500, 6300, 8400, 12590, 15120, 11340, 9450]
value = 199970
# for i in it.combinations(list1, 2):
#     print(i)
start = time.perf_counter()
A = it.combinations(list1, 10)
A = np.array(list(A))
# A_sum = np.sum(A, axis=1, keepdims=True)
A_sum = np.sum(A, axis=1)
A_sum_sort = np.sort(A_sum, axis=0)
A_min = np.abs(A_sum - value).argmin()
A_abs = np.abs(A_sum - value)
A_winner = np.argwhere(A_abs == np.amin(A_abs))
print(A)
print(A_sum)
print(A_sum_sort)
print("min:", A_min, ",list:", A_sum[A_min], ",A:", A[A_min])
print("A_winner:", A_winner.flatten())
print("A_sum with the winner:", A[A_winner.flatten()])
stop = time.perf_counter()
print("running time:", stop - start)
