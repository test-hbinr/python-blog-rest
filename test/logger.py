#!/usr/bin/python
# -*- coding: UTF-8 -*-
# @License: Copyright © 2019 代鹏. All rights reserved.
# @Author: 代鹏
# @Contact: 272481080@qq.com
# @File: cu_logger.py
# @Date: 2021/11/11
# @Software：kk
# @description : 日志格式化

import logging
from logging import handlers
from app import app
import sys

def exec(name):
    # logging.basicConfig(level=app.config.get('LOG_LEVEL'),
    #                     filename=app.config.get('LOG_FILENAME'),
    #                     filemode=app.config.get('LOG_FILEMODE'),
    #                     format=app.config.get('LOG_FORMAT')
    #                     )
    # logging.basicConfig(level=logging.WARNING, filename='./cu-basic.log')
    # format_str = logging.Formatter(app.config.get('LOG_FORMAT'))  # 设置日志格式
    format_str = logging.Formatter('%(asctime)s - %(levelname)s: %(message)s')  # 设置日志格式
    # logger.setLevel(app.config.get('LOG_LEVEL'))  # 设置日志级别
    #
    th = handlers.TimedRotatingFileHandler(filename='./cu-basic.log', when='D', backupCount=0,
                                           encoding='utf-8')  # 往文件里写入#指定间隔时间自动生成文件的处理器
    # 实例化TimedRotatingFileHandler
    # interval是时间间隔，backupCount是备份文件的个数，如果超过这个个数，就会自动删除，when是间隔的时间单位，单位有以下几种：
    # S 秒
    # M 分
    # H 小时、
    # D 天、
    # W 每星期（interval==0时代表星期一）
    # midnight 每天凌晨
    th.setFormatter(format_str)  # 设置文件里写入的格式
    # th.setLevel(logging.INFO)
    # sh = logging.StreamHandler(sys.stdout)#往屏幕上输出
    # console = logging.StreamHandler()  # 往屏幕上输出
    # console.setFormatter(format_str)  # 设置屏幕上显示的格式
    # console.setLevel(app.config.get('LOG_LEVEL'))
    # self.logger.addHandler(console)  # 把对象加到logger里

    app.logger.addHandler(th)
    # app.logger.addHandler(console)
    # console.close()
    # th.close()
    app.logger.setLevel(app.config.get('LOG_LEVEL'))
    app.logger.info('info')
    app.logger.warning('警告')


if __name__ == '__main__':
    # log.debug('debug')
    # log.info('info')
    # log.warning('警告')
    # log.error('报错')
    # Logger().logger.critical('严重')
    # Logger().logger.error('error')
    exec(__name__)
