# -*- coding: UTF-8 -*-
import os
from .default import Config
basedir = os.path.abspath(os.path.dirname(__file__))


class ProductionConfig(Config):
    UPLOAD_FOLDER = '/usr/local/nginx/html/source'
    LOG_FILENAME = '/home/python_home/logs/cu.log'

