# -*- coding: UTF-8 -*-
import os
import logging
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    WTF_CSRF_ENABLED = True
    SECRET_KEY = 'you-will-never-guess'

    # JSON_AS_ASCII = False
    ES_HOST = '10.0.97.122'
    ES_PORT = '9200'

    MONGODB_HOST = '127.0.0.1'
    MONGODB_PORT = 27017
    MONGODB_DB = 'cu'
    MONGODB_USERNAME = None
    MONGODB_PASSWORD = None

    UPLOAD_FOLDER = 'D:\\temp'
    PREFIX_ATTACHED = 'source/'
    # 控制台打印的日志级别WARNING,INFO
    LOG_LEVEL = logging.INFO
    LOG_FILENAME = './cu.log'
    # 模式，有w和a，w就是写模式，每次都会重新写日志，覆盖之前的日志
    # a是追加模式，默认如果不写的话，就是追加模式
    # LOG_FILEMODE = 'a'
    # 日志格式
    LOG_FORMAT = '%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s'

