# -*- coding: UTF-8 -*-
import os


def load_config():
    """加载配置类"""
    mode = os.environ.get('MODE') or ''
    print("ENV MODE:" + mode)
    try:
        if mode == 'PRODUCTION':
            from .production import ProductionConfig
            return ProductionConfig
        elif mode == 'DEVELOPMENT':
            from .testing import TestingConfig
            return TestingConfig
        else:
            from .development import DevelopmentConfig
            return DevelopmentConfig
    except ImportError:
        from .default import Config
        return Config
